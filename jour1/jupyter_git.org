#+TITLE: Coder, versionner et collaborer avec Git et JupyterLab
#+AUTHOR: Christophe Halgand, Frédéric Santos
#+EMAIL: frederic.santos@u-bordeaux.fr
#+DATE: 15 novembre 2021
#+STARTUP: showall num
#+OPTIONS: email:t toc:t ^:nil
#+LATEX_HEADER: \usepackage[natbibapa]{apacite}
#+LATEX_HEADER: \usepackage[french]{babel}
#+LATEX_HEADER: \usepackage{a4wide}
#+LATEX_HEADER: \usepackage{mathpazo}
#+LATEX_HEADER: \usepackage[matha,mathb]{mathabx}
#+LATEX_HEADER: \usepackage{booktabs}
#+LATEX_HEADER: \usepackage{amsmath}
#+LATEX_HEADER: \usepackage{titlesec}
#+LATEX_HEADER: \titlelabel{\thetitle.\quad}
#+LATEX_HEADER: \usepackage[usenames,dvipsnames]{xcolor} % For colors with friendly names
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage{mdframed}                    % Companion of minted for code blocks
#+LATEX_HEADER: \usepackage{fancyvrb}                    % For verbatim R outputs
#+LATEX_HEADER: \usemintedstyle{friendly} % set style if needed, see https://frama.link/jfRr8Lpj
#+LATEX_HEADER: \mdfdefinestyle{mystyle}{linecolor=gray!30,backgroundcolor=gray!30}
#+LATEX_HEADER: \BeforeBeginEnvironment{minted}{%
#+LATEX_HEADER: \begin{mdframed}[style=mystyle]}
#+LATEX_HEADER: \AfterEndEnvironment{minted}{%
#+LATEX_HEADER: \end{mdframed}}
#+LATEX_HEADER: %% Formatting of verbatim outputs (i.e., outputs of R results):
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{%
#+LATEX_HEADER:   fontsize = \small,
#+LATEX_HEADER:   frame = leftline,
#+LATEX_HEADER:   formatcom = {\color{gray!97}}
#+LATEX_HEADER: }
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{url}
#+LATEX_HEADER: %% For DOI hyperlinks in biblio:
#+LATEX_HEADER: \usepackage{doi}
#+LATEX_HEADER: \renewcommand{\doiprefix}{}
#+LANGUAGE: fr

#+begin_export latex
\pagebreak
#+end_export

* Objectifs
Ce guide vous permettra d'utiliser efficacement JupyterLab (et son plugin ~@jupyterlab/git~) pour écrire et exécuter vos scripts, les versionner régulièrement, et interagir avec GitUB (ou toute autre plateforme collaborative telle que [[https://github.com/][Github]] ou [[https://gitlab.com/][GitLab]]) afin d'y entreposer votre travail et de collaborer à plusieurs sur ce dépôt.

* Versionner : pourquoi, comment ?
** Principes
Le travail de recherche est généralement incrémental et collaboratif, ce qui pose un double problème :
1. Au fur et à mesure de l'avancée du travail, de nouvelles versions des données, des scripts, de la documentation et des résultats sont produites. Lorsque ces montées de versions sont nommées "artisanalement" à l'aide de nomenclatures telles que ~scipt_v3.R~ ou ~data_2021_06_03.csv~, ces fichiers cohabitent sur le disque dur, sans réellement avoir suivi la nature des changements d'une version à l'autre. De plus, il peut devenir difficile de savoir quelles versions du script et des données sont associées à chaque version intermédiaire des résultats. Le projet dans son ensemble devient désordonné, difficilement reproductible, et sa progression peu traçable.
2. L'aspect collaboratif pose un problème supplémentaire : les multiples versions de ces fichiers sont alors envoyées à tous les collaborateurs, qui peuvent de leur côté les modifier à leur tour, et souvent de façon simultanée. Il devient alors pratiquement impossible de déterminer quelle est exactement la "dernière" ou la "bonne" version des fichiers, et qui en est le détenteur. De plus, la gestion des conflits d'édition simultanée sur les mêmes parties des fichiers devient extrêmement laborieuse, et sujette à de nombreuses erreurs.

Les systèmes de gestion de versions avancés, tels que Git, SVN ou Mercurial, permettent de résoudre ces problèmes, notamment grâce à :
- une traçabilité parfaite de toutes les modifications effectuées sur les fichiers (qui a modifié quoi, et quand) ;
- l'existence d'outils de centralisation du projet, facilitant le travail collaboratif en ligne ;
- une capacité à résoudre les conflits d'édition simultanée ;
- la possibilité pour chaque collaborateur d'explorer et d'expérimenter librement sans altérer l'état global du projet, par exemple grâce au système de /branches/ offert par Git.

Dans cette formation, nous utiliserons [[https://git-scm.com/][Git]] comme système de versionnement, et la plateforme [[https://gitlab.com/][GitLab.com]] pour les synchronisations distantes.

** Dépôts distants
En plus de l'intérêt d'un versionnement "en local" (pour soi-même), Git facilite également la collaboration à plusieurs sur un projet, grâce à l'utilisation de /dépôts distants/. Plusieurs plateformes (Github, GitLab, ...) proposent de synchroniser votre projet sur un espace en ligne, auquel peuvent accéder vos collaborateurs, et possiblement le monde entier.

#+CAPTION: Principe de l'utilisation de dépôts distants.
#+ATTR_LATEX: :width 0.66 \textwidth
[[./images/principe_git.png]]

** Quelques ressources additionnelles
- Une excellente /propagande/ pour le versionnement, plus détaillée que la présentation rapide effectuée ci-dessus, peut être trouvée dans cite:desquilbet2019_VersRechercheReproductible.
- Le fonctionnement de Git, quant à lui, est exhaustivement illustré dans cite:chacon2014_ProGit.
- De nombreuses publications récentes fournissent également des recommandations complètes sur la façon d'intégrer le versionnement et la programmation lettrée au /workflow/ d'un projet de recherche [[citep:marwick2017_ComputationalReproducibilityArchaeological,alston2021_BeginnerGuideConducting,stanisic2015_EffectiveGitOrgMode][e.g.,::]].

Noter qu'il existe également des sites web proposant de petits exercices pratiques pour s'initier de façon ludique aux principes de Git et du versionnement. On pourra notamment citer :
- Learn Git branching : https://learngitbranching.js.org/ ;
- GitExercises : https://gitexercises.fracz.com/.

* Configurer et utiliser un dépôt GitLab
Durant cette formation, vous versionnerez et synchroniserez régulièrement vos fichiers sur GitLab.com, avec le double objectif de vous habituer à cette bonne pratique, et de permettre aux formateurs de suivre plus aisément l'avancée de votre travail.

** Créer votre compte GitLab
Si vous ne possédez pas encore de compte sur GitLab.com, visitez le lien suivant afin d'en créer un :
#+begin_center
\url{https://gitlab.com/users/sign_up}
#+end_center

** Faire un /fork/ du dépôt de la formation
label:sec-fork
Un dépôt contenant tous les documents et toutes les données nécessaires pour la formation est disponible sur GitLab à l'adresse suivante (pour le parcours R) :

   #+begin_center
\url{https://gitlab.com/f-santos/anf-cnrs-ds-r}
   #+end_center

Chaque participant devra faire un /fork/ de ce dépôt, c'est-à-dire en faire une copie qui lui sera propre et sur laquelle il sera possible de travailler sans interférer avec la progression des autres participants. Pour faire un /fork/ du dépôt de la formation, rendez-vous sur la page du dépôt et cliquez sur le bouton "Créer une divergence" en haut à droite. Validez ensuite la création de cette divergence (cela pourra prendre quelques secondes avant qu'elle ne soit effectivement créée). Vous devriez alors être redirigé vers la page de votre divergence personnelle, où figure une copie de tous les fichiers du dépôt originel.

** Cloner localement ce dépôt
Même s'il existe une manière d'éditer (de façon très basique) des fichiers texte directement sur l'interface en ligne de GitLab, la démarche classique consiste plutôt à /cloner/ localement ce dépôt, c'est-à-dire à le télécharger sur votre ordinateur, et à travailler directement sur cette copie locale. Par la suite, vos changements pourront être /poussés/ vers le dépôt distant GitLab, de telle sorte que le dépôt distant et sa copie locale soient synchronisés.

Un peu de configuration sera nécessaire avant cela.

*** Créer une clé SSH
Pour assurer la sécurité des communications entre votre copie locale et le dépôt distant (et les faciliter), commençons par créer une clé SSH.

1. Ouvrir une console système (pour Linux ou Mac OS), ou une console Git Bash (pour Windows).
2. Exécuter alors dans cette console l'instruction suivante, en adaptant avec votre propre adresse mail :
   #+begin_src bash :eval no :exports code
ssh-keygen -t rsa -C "votre-email@univ.fr"
   #+end_src
3. Si l'on vous demande "Enter a file in which to save the key", appuyez simplement sur Entrée pour accepter le choix par défaut[fn::Une exception notable : si vous avez déjà plusieurs clés SSH associées à différents comptes, vous ne pourrez pas garder ici le choix par défaut. Dans ce cas, voir directement avec le formateur.].
4. Vous avez ensuite l'option (facultative) d'entrer une /passphrase/ pour davantage de sécurité. Afin de s'épargner d'autres étapes de configuration fastidieuses, nous vous recommandons, pour cette formation, de ne pas saisir de /passphrase/ et de simplement valider par Entrée.

*** Renseigner votre clé SSH sur GitLab
Cette clé SSH devra être connue de GitLab afin de permettre la synchronisation de votre dépôt. Pour cela :

1. Copiez dans le presse-papier votre clé SSH. Par exemple :
   + Utilisateurs de Linux (dans un terminal) :
     #+begin_src bash :eval no :exports code
xclip -sel clip < ~/.ssh/id_rsa.pub
     #+end_src
   + Utilisateurs de Mac OS (dans une console système) :
     #+begin_src bash :eval no :exports code
tr -d '\n' < ~/.ssh/id_rsa.pub | pbcopy
     #+end_src
   + Utilisateurs de Windows (dans une console Git Bash) :
     #+begin_src bash :eval no :exports code
cat ~/.ssh/id_rsa.pub | clip
     #+end_src
2. Revenez sur l'interface en ligne de GitLab.
3. Cliquez sur votre avatar en haut à droite, puis sur "Preferences".
4. Dans la barre de menu à gauche, cliquez sur "SSH Keys" (ou "Clefs SSH").
5. Dans la boîte de saisie "key", collez votre clé.
6. Dans la boîte de saisie "Title", écrivez une description, telle que "PC portable".

*** Cloner votre dépôt et configurer Git
Vous pouvez à présent cloner votre dépôt localement sur votre disque dur.

1. Sur GitLab, rendez-vous sur la divergence (/fork/) précédemment créée en Section ref:sec-fork.
2. Cliquez sur le bouton "Clone", et copiez l'adresse correspondant au protocole SSH (Fig. ref:fig-clone-ssh).
   #+CAPTION: Cloner votre dépôt par protocole SSH. label:fig-clone-ssh
   #+ATTR_LATEX: :width 0.65 \textwidth
   [[./images/clone_ssh.png]]
3. Ouvrez une console système (Linux ou Mac OS), ou une console Git Bash (Windows). Naviguez (à l'aide de la commande ~cd~) vers un endroit où vous souhaitez cloner le dépôt Git sur votre disque dur, puis lancez la commande =git clone=. Par exemple (pour un utilisateur de Windows), si vous souhaitez cloner votre dépôt dans la racine du répertoire =Documents=, vous pouvez faire :
   #+begin_src bash :exports code :eval no
cd ~/Documents/
git clone git@gitlab.com:XXXXXXXXXXXXXXXXXXXX
   #+end_src
   où vous aurez préalablement pris soin de remplacer XXXXXXXXXXXXXX par l'adresse réelle de votre /fork/.
4. Une fois le téléchargement terminé, configurez Git en exécutant (en les adaptant) les instructions suivantes dans une console système (après vous être placé dans le répertoire Git que vous venez de cloner).
   #+begin_src bash :eval no :exports code
# Renseigner votre identité :
git config --local user.name "votre-id"
git config --local user.email "votre-email@univ.fr"

# Autoriser Git à conserver (localement) vos ID de connexion :
git config credential.helper store
   #+end_src

Grâce à cette dernière étape, GitLab sera capable de mémoriser vos informations de connexion et ne vous les redemandera pas à chaque tentative de synchronisation. Afin d'obtenir plus d'informations sur toutes les options de configuration de Git, vous pouvez consulter la page d'aide d'intégrée en exécutant cette instruction dans une console :

#+begin_src shell :eval no :exports code
man git-config
#+end_src

ou consulter la page d'aide en ligne sur la persionnalisation de Git (https://git-scm.com/book/fr/v2/Personnalisation-de-Git-Configuration-de-Git).

* Utiliser Git à travers JupyterLab
** Principe et /workflow/
Une fois que votre dépôt Git est cloné localement, vous pouvez travailler dans JupyterLab de façon incrémentale, en ajoutant/modifiant progressivement du code, de la documentation, des données, des résultats... À chaque fois que vous pensez avoir atteint un état "suffisamment remarquable" de votre travail, vous pouvez alors prendre un /snapshot/ de l'état actuel de votre répertoire de travail. Pour cela, vous devrez :
1. Sélectionner ("placer dans la /staging area/") les modifications qui devront être présentes dans ce snapshot. En effet, Git compare l'état actuel de votre travail avec le précédent snapshot disponible : vous pouvez inclure dans le futur snapshot toutes les modifications opérées depuis, ou seulement une partie d'entre elles (par exemple pour découper vos modifications en plusieurs snapshots séparés).
2. Sauvegarder l'état actuel du répertoire en tenant compte des modifications sélectionnées ("faire un /commit/" de vos fichiers). Ajoutez alors un descriptif de ce commit, de façon à constituer un historique aisément lisible des modifications opérées.
3. (Facultatif) Synchroniser (/push/) votre répertoire local avec le dépôt GitLab distant. Reprenez alors votre travail... jusqu'au prochain commit !

Une extension JupyterLab, ~jupyterlab-git~, facilite l'ensemble de ces opérations en les intégrant directement à l'interface graphique de JupyterLab.

** Ouvrir JupyterLab
Pour démarrer JupyterLab :
- Utilisateurs de Linux : après vous êtres placé dans le répertoire de votre /fork/ personnel du dépôt Git, exécutez la commande =jupyter lab= dans une console système.
- Utilisateurs de Windows : tapez =Anaconda= dans la zone de recherche de la barre des tâches, puis lancez l'Anaconda Navigator. Dans la page d'accueil, sélectionnez Jupyter Lab (Fig. ref:fig-anaconda).

#+CAPTION: Anaconda Navigator. label:fig-anaconda
#+ATTR_LATEX: :width 0.8 \textwidth
[[./images/anaconda.png]]

Dans le navigateur de fichiers à gauche de l'interface JupyterLab, naviguez jusqu'à votre /fork/ personnel du dépôt de la formation.

** Créer un nouveau fichier dans notre répertoire
Créons à présent un nouveau fichier dans notre répertoire de travail à l'aide de JupyterLab. Imaginons que vous souhaitiez créer prendre des notes durant la formation, dans un fichier =notes.md= (ou plusieurs fichiers =jour1.md=, =jour2.md=, etc., stockés dans un dossier spécifique). Une manière simple et légère peut être d'utiliser le langage Markdown, langage de balisage léger dont quelques éléments de syntaxe[fn::Pour des compléments, de nombreux tutoriels markdown existent sur le web, permettant de maîtriser très vite cette syntaxe. Un exemple : https://www.markdowntutorial.com/.] peuvent être trouvés en Table ref:tab-markdown). Créons à présent notre premier fichier =notes.md=, que l'on pourra éventuellement placer dans un dossier dédié.

#+CAPTION: Quelques éléments de syntaxe markdown. label:tab-markdown
#+ATTR_LATEX: :booktabs t
|----------------------+---------------------------------------------|
| Élément              | Syntaxe                                     |
|----------------------+---------------------------------------------|
| Italique             | =*texte*=                                   |
| Gras                 | =**texte**=                                 |
| Titre                | =# Titre de niveau 1=                       |
| Sous-titre           | =## Titre de niveau 2=                      |
| Mise en forme "code" | =`code`=                                    |
| Lien                 | =[texte](https://www.adresse-du-lien.com)=  |
|----------------------+---------------------------------------------|

Pour cela, sur l'écran d'accueil ("Laucnher") Jupyter Lab, choisissez l'option "Markdown file", ajoutez un peu de contenu à votre fichier de notes, puis sauvegardez-le (par exemple sous le nom =notes.md=, dans un dossier =notes/= de votre /fork/ personnel).

** Placer des fichiers dans la /staging area/
Nous disposons désormais localement d'une copie /modifiée/, enrichie, de notre /fork/ personnel par rapport à la version que nous avions clonée. Nous souhaitons à présent prendre en compte ces modifications dans le système de versionnement.

Cliquez sur l'icône Git dans la barre d'outils JupyterLab (entourée en rouge sur la Figure ref:fig-premier-fichier). Il existe trois statuts possibles pour les fichiers présents dans le répertoire :
- /untracked/ : fichiers nouvellement créés depuis le dernier commit, et donc totalement inconnus de Git à ce stade ;
- /changed/ : fichiers modifiés depuis le dernier commit ;
- /staged/ : fichiers qui feront partie du prochain commit.

Le fichier =notes.md= nouvellement créé est actuellement /untracked/. Pour l'ajouter au prochain commit, cliquez sur l'icône "+" à côté du fichier (entourée en vert sur la Figure ref:fig-premier-fichier). Vous devriez alors voir apparaître ce fichier dans la rubrique /Staged/ : vous venez ainsi d'ajouter ce notebook dans la /staging area/ des modifications qui prendront part au prochain commit.

#+CAPTION: Ajouter le fichier =notes.md= dans la /staging area/. label:fig-premier-fichier
#+ATTR_LATEX: :width 0.89 \textwidth
[[./images/premier-fichier.png]]

/Remarque/. --- JupyterLab crée régulièrement et automatiquement ses propres /checkpoints/ pour chaque notebook que vous créez. Usuellement, il n'est pas utile de les considérer ou de les inclure dans vos snapshots. Vous pouvez donc les laisser éternellement en statut /untracked/. Pour ne plus les voir apparaître dans cette rubrique, vous pouvez utiliser un fichier ~.gitignore~, dans lequel vous pouvez lister tous les fichiers qui doivent être totalement ignorés par Git. On y place usuellement (au moins) tous les fichiers automatiquement générés par diverses applications, tous les fichiers temporaires, etc. Voir la documentation en ligne (https://git-scm.com/docs/gitignore) à ce sujet.

** Effectuer des commits
Lorsque vous effectuez un /commit/ dans votre dépôt, Git prend un instantané (/snapshot/) de l'état global du dépôt, et regarde les modifications effectuées depuis le dernier snapshot. C'est grâce à ce système que vous pouvez conserver un historique précis et riche de l'avancée de votre travail : il est toujours possible de revenir à l'état défini par un snapshot antérieur, et/ou de consulter toutes les modifications faites dans un fichier entre deux snapshots donnés.

Vous serez donc amenés à effectuer régulièrement des commits : au moins à chaque changement suffisamment important pour mériter une trace dans votre historique de modifications. (Selon la sensibilité personnelle, cela peut aller d'une simple correction de typo à des modifications plus substantielles.)

Vous êtes à présent prêt à effectuer un commit, afin d'inclure le fichier =notes.md= dans votre projet. Pour cela :
1. Écrivez un descriptif concis mais explicite de votre commit dans le champ idoine sur JupyterLab (entouré en rouge sur la Figure ref:fig-premier-commit).
2. Cliquez sur le bouton "Commit" en bas de l'écran.

#+CAPTION: Préparation de votre premier commit via JupyterLab. label:fig-premier-commit
#+ATTR_LATEX: :width \textwidth
[[./images/premier-commit.png]]

Après un court temps de chargement, JupyterLab confirme alors la réussite du commit, qui est à présent prêt à être synchronisé avec le dépôt distant sur GitLab.

** Synchroniser les modifications locales avec le dépôt distant GitLab
Lorsque vous avez opéré localement des modifications que vous n'avez pas encore synchronisées avec le dépôt GitLab distant, JupyterLab l'indique par une icône spécifique (entourée sur la Figure ref:fig-push-changes). Cliquez sur cette icône afin de pousser vers le dépôt GitLab les modifications enregistrées lors de votre dernier commit. Vous disposerez toujours d'une copie (en ligne, et à jour) de votre travail, copie à laquelle vos collaborateurs peuvent également accéder.

#+CAPTION: Synchronisation avec le dépôt distant. label:fig-push-changes
#+ATTR_LATEX: :width 0.7 \textwidth
[[./images/push_changes.png]]

* Biblio                                                             :ignore:
bibliography:biblio.bib
bibliographystyle:apacite
