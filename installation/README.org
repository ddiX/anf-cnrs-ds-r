#+TITLE: Instructions d'installation
#+AUTHOR: Christophe Halgand, Frédéric Santos

** Pour l'environnement de travail au sens large
Vous trouverez ici les instructions détaillées en fonction de votre système d'exploitation :
- [[https://gitub.u-bordeaux.fr/fsantos/anf-cnrs-ds-r/-/blob/master/installation/linux.org][pour Linux]] ;
- [[https://gitub.u-bordeaux.fr/fsantos/anf-cnrs-ds-r/-/blob/master/installation/windows.org][pour Windows]].

Les utilisateurs de systèmes Mac OS peuvent s'inspirer de la documentation Linux et Windows, et contacter le formateur directement par mail si nécessaire.

** Pour R
Quel que soit votre système d'exploitation, nous partons du principe que vous possédez la dernière version de R (>= 4.1.1). Afin de gagner du temps lors de la formation, il est préférable d'installer en amont les packages requis en exécutant simplement le script suivant (par exemple dans Rstudio) :
#+begin_src R :eval no :exports code
install.packages("groundhog", repos = "https://cloud.r-project.org/")
## Import des packages requis :
library(groundhog)                             # favorise la reproductibilité
today <- "2021-10-15"                          # date de version des packages
groundhog.library("beeswarm", date = today)    # représentation graphique
groundhog.library("data.table", date = today)  # chargement rapide de gros fichiers
groundhog.library("fs", date = today)          # outils divers de manip de fichiers
groundhog.library("here", date = today)        # gestion des chemins dans un projet
groundhog.library("nbconvertR", date = today)  # export de notebooks .ipynb -> .R
groundhog.library("repr", date = today)        # pour les graphiques sous Jupyter
groundhog.library("RJSONIO", date = today)     # import de fichiers .json
## Quelques packages additionnels :
groundhog.library(
    pkg = "dplyr",
    date = today,
    ignore.deps = c("crayon", "lifecycle", "rlang", "pillar")
)
groundhog.library(
    pkg = "plotly",
    date = today,
    ignore.deps = c("crayon", "lifecycle", "rlang", "pillar")
)
groundhog.library("textshape", date = today)
groundhog.library("tidyr", date = today)
#+end_src
