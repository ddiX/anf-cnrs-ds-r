#+TITLE: Programme du 18/11/2021

Session en groupes séparés ; on décrit ici le parcours R uniquement.

** Objectifs
- Découverte de l'environnement Jupyter
- Rédaction des premiers /notebooks/
- Mise en place de l'infrastructure R
- Prise en main du jeu de données
- Versionner régulièrement son travail et le synchroniser avec GitUB

** Rapide descriptif des notebooks
- =01_workflow_jupyter.ipynb= : prendre en main Jupyter, ses plugins, ainsi que les packages R qui seront utilisés durant la formation
- =02_orga_data.ipynb= : découvrir l'organisation et la structure des données
- =03_load_data.ipynb= : charger une grande masse de données organisées dans de multiples fichiers et dossiers, et les réunir en des dataframes "propres"
- =04_explo_analytic.ipynb= : explorer le premier de ces deux dataframes finaux
- =05_explo_tempo.ipynb= : explorer le second de ces deux dataframes finaux

